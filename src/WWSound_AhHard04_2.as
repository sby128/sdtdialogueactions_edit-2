package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_AhHard04_2.swf",symbol="WWSound_AhHard04_2")]
	
	public dynamic class WWSound_AhHard04_2 extends Sound {
		
		public function WWSound_AhHard04_2() {
			super();
		}
	}

}
