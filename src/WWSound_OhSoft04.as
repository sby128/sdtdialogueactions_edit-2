package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_OhSoft04.swf",symbol="WWSound_OhSoft04")]
	
	public dynamic class WWSound_OhSoft04 extends Sound {
		
		public function WWSound_OhSoft04() {
			super();
		}
	}

}
