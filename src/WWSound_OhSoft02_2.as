package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_OhSoft02_2.swf",symbol="WWSound_OhSoft02_2")]
	
	public dynamic class WWSound_OhSoft02_2 extends Sound {
		
		public function WWSound_OhSoft02_2() {
			super();
		}
	}

}
