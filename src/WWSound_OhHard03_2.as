package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_OhHard03_2.swf",symbol="WWSound_OhHard03_2")]
	
	public dynamic class WWSound_OhHard03_2 extends Sound {
		
		public function WWSound_OhHard03_2() {
			super();
		}
	}

}
