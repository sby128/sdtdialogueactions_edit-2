package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_OhHard02_2.swf",symbol="WWSound_OhHard02_2")]
	
	public dynamic class WWSound_OhHard02_2 extends Sound {
		
		public function WWSound_OhHard02_2() {
			super();
		}
	}

}
