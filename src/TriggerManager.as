package  
{
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author Pimgd
	 */
	public class TriggerManager 
	{
		private var g:Object;
		private var registeredTriggers:Dictionary;
		public function TriggerManager(G:Object) 
		{
			g = G;
			registeredTriggers = new Dictionary();
		}
		
		//Note to self:
		//SDT does pause, nextWord, trigger.
		//I do trigger, nextWord.
		public function trigger():Boolean {
			var triggerName:String = g.dialogueControl.words[g.dialogueControl.sayingWord].word;
			return triggerFromString(triggerName);
		}
		
		public function triggerFromString(triggerName:String):Boolean {
			//dialogueLog("Trigger found: = " + triggerName);
			triggerName = triggerName.substring(1, triggerName.length - 1);
			var triggerWithArguments:Array = triggerName.split("_");
			var result:Boolean = triggerWithArray(triggerWithArguments);
			if (result) {
				g.dialogueControl.nextWord();
			}
			return result;
		}
		
		public function triggerFromStringAPI(input:String):Boolean {
			//dialogueLog("Trigger found: = " + triggerName);
			if (StringFunctions.stringStartsWith(input, "[") && StringFunctions.stringEndsWith(input, "]")) {
				var result:Boolean = undefined;
				do {
					var singleTriggerName:String = input.substring(1, input.indexOf("]"));
					result = triggerFromStringAPI(singleTriggerName) && result;
					input = input.substring(input.indexOf("]") + 1);
				} while (input != "" && StringFunctions.stringStartsWith(input, "[") && StringFunctions.stringEndsWith(input, "]"));
				return result;
			}
			var triggerWithArguments:Array = input.split("_");
			return triggerWithArray(triggerWithArguments);
		}
		
		private function triggerWithArray(triggerSplitAsArray:Array):Boolean {
			if (triggerSplitAsArray.length == 0) {
				return false;
			}
			
			var triggerFunction:FunctionObject = null;
			var name:String = triggerSplitAsArray[0];
			var triggerArray:Array = triggerSplitAsArray.concat();
			triggerArray.splice(0, 1);
			
			triggerFunction = getFunctionObjectForTrigger(name, -1);
			if (triggerFunction != null) {
				logTriggerFunctionCall(name, triggerArray);
				triggerFunction.call(triggerArray);
				return true;
			}
			
			triggerFunction = getFunctionObjectForTrigger(name, triggerArray.length);
			if (triggerFunction == null) {
				if (triggerArray.length == 0) {
					return false;
				} 
				triggerArray[0] = name + "_" + triggerArray[0];
				return triggerWithArray(triggerArray);
			}
			
			logTriggerFunctionCall(name, triggerArray);
			triggerFunction.call(triggerArray);
			return true;
		}
		
		private function logTriggerFunctionCall(name:String, args:Array):void {
			var debugString:String = "";
			for (var i:uint = 0; i < args.length; i++) {
				if (i != 0) {
					debugString += ",";
				}
				debugString += args[i];
			}
			dialogueLog("Trigger called: " + name + " with " + args.length + " arguments (" + debugString + ")");
		}
		
		public function registerTriggerComms(name:String, argumentCount:int, callbackFunction:Function, callbackTarget:Object, callbackArgs:Array):void {
			registerTrigger(name, argumentCount, new FunctionObject(callbackFunction, callbackTarget, callbackArgs));
		}
		
		public function registerTrigger(name:String, argumentCount:int, callback:FunctionObject):void {
			if (!hasTrigger(name, argumentCount)) {
				var triggerArray:Array = registeredTriggers[name];
				if (triggerArray == null) {
					triggerArray = new Array();
					registeredTriggers[name] = triggerArray;
				}
				triggerArray[argumentCount] = callback;
			} else {
				throw new Error("IllegalStateException: Already have such a trigger ["+name+"] with "+argumentCount+" arguments.");
			}
		}
		
		public function hasTrigger(name:String, argumentCount:int):Boolean {
			var callbackFunction:FunctionObject = getFunctionObjectForTrigger(name, argumentCount);
			return (callbackFunction != null);
		}
		
		public function getFunctionObjectForTrigger(name:String, argumentCount:int):FunctionObject {
			var hasTrigger:Boolean = registeredTriggers.hasOwnProperty(name);
			if (hasTrigger) {
				var triggerArray:Array = registeredTriggers[name];
				if (triggerArray == null) {
					triggerArray = new Array();
					registeredTriggers[name] = triggerArray;
				}
				return triggerArray[argumentCount];
			} else {
				return null;
			}
		}
		
		private function dialogueLog(message:String):void {
			g.dialogueControl.advancedController.outputLog("DialogueActions(TriggerManager): "+message);
		}
		
	}

}