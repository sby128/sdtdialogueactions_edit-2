package  
{
	import flash.events.Event;
	/**
	 * ...
	 * @author Pimgd
	 */
	public class LineTypeManager 
	{
		private var g:Object;
		private var m:Main;
		private var dialogueStateClass:Class;
		private var startDialogue:Boolean = false;
		private var passOutDialogue:Boolean = false;
		private var chokeDialogue:Boolean = false;
		private var startDialoguePriority:Number = 3;
		private var startDialogueDelay:Number = 60;
		private var startDialogueDone:Boolean = false;
		private var orgasmDialoguePriority:Number = 3;
		private var passOutDialoguePriority:Number = 3;
		
		//X Added by WeeWillie 2/21/14
		private var wwButtonDialoguePriority:Number = 4;
	  
		private var dialogueClearDelay:int = 300;
		public function LineTypeManager(G:Object, M:Main) 
		{
			g = G;
			m = M;
			dialogueStateClass = m.getLoaderRef().eDOM.getDefinition("obj.dialogue.DialogueState") as Class;
		}
		
		public function onDialogueLoad():void {
			cleanDialogue();
			if (g.dialogueControl.library.getPhrases("start").length >= 1) {
				g.dialogueControl.states["start"] = new dialogueStateClass(180, startDialoguePriority);
				startDialogue = true;
				startDialogueDone = false;
			}
			
			if (g.dialogueControl.library.getPhrases("orgasm").length >= 1) {
				g.dialogueControl.states["orgasm"] = new dialogueStateClass(240, orgasmDialoguePriority);
				
			}
			if (g.dialogueControl.library.getPhrases("passed_out").length >= 1) {
				g.dialogueControl.states["passed_out"] = new dialogueStateClass(240, passOutDialoguePriority);
				passOutDialogue = true;
			}
			if (g.dialogueControl.library.getPhrases("choking").length >= 1) {
				g.dialogueControl.states["choking"] = new dialogueStateClass(240, passOutDialoguePriority);
				chokeDialogue = true;
			}
			//X Added by WeeWillie 2/21/14
			for (var i:uint = 1; i <= 10; i++) { 
				if (g.dialogueControl.library.getPhrases("button"+i).length >= 1) {
					g.dialogueControl.states["button" + i] = new dialogueStateClass(240, this.wwButtonDialoguePriority);
				}
			}
		}
		
		public function buildDialogueStates(e:Event = null):void {
			if (dialogueClearDelay == 0) {
				cleanDialogue();
			}
			dialogueClearDelay--;
			if (startDialogue && !startDialogueDone && (!g.gamePaused && g.gameRunning)) {
				onDialogueLoad();
				if (startDialogue && startDialogueDelay <= 0) {
					var lineToPlay:* = g.dialogueControl.library.getPhrases("start")[
							g.dialogueControl.states["start"].randomPhrase(
								g.dialogueControl.library.getPhrases("start")
							)
						];//obj.dialogue.DialogueLine
					if (lineToPlay != null) {
						g.dialogueControl.startSpeakingPhrase(lineToPlay);
					}
					startDialogueDone = true;
					resetDialogueClearDelay();
				} else {
					startDialogueDelay--;
				}
			}
			if (passOutDialogue && g.her.passedOut) {
				g.dialogueControl.buildState("passed_out", 120);
				resetDialogueClearDelay();
			}
			if (chokeDialogue && !g.her.passedOut && g.her.passOutFactor != 0 && g.her.mouthFull) {
				g.dialogueControl.buildState("choking", 120);
				resetDialogueClearDelay();
			}
		}
		
		public function triggerLine(lineType:String, priority:Number):void {
			g.dialogueControl.buildState(lineType, priority);
		}
		
		public function cleanDialogue():void {
			if (!g.dialogueControl.showingText) {
				for (var state in g.dialogueControl.states) {
					if (g.dialogueControl.library.getPhrases(state).length == 0 && g.dialogueControl.states[state].build > 0) {
						g.dialogueControl.states[state].clearBuild(); //cleans up built up dialog that don't have lines for
					}
				}
			}
			resetDialogueClearDelay();
		}

		public function resetDialogueClearDelay():void {
			dialogueClearDelay = 200;
		}
		
		private function dialogueLog(message:String):void {
			g.dialogueControl.advancedController.outputLog("DialogueActions(LineTypeManager): "+message);
		}
	}

}