v4.09
[PUSSYDRIP] - Effectively [PUSSYDRIP_50].
[SPLITSTRING_stringLiteral_localTargetObjectVariable] trigger. Assigns the length of the string literal to targetObjectVariable.length, and each of its individual characters to targetObjectVariable.charAt.<number>. Strings are 0 indexed, so "apple" will turn into charAt.0=a, charAt.1=p, charAt.2=p, charAt.3=l, charAt.4=e, length=5
[SPLITSTRINGBYNAME_localVariable_localTargetObjectVariable] trigger. Performs SPLITSTRING on the contents of localVariable.
[SPLITSTRINGBYNAEM_localVariable_localTargetObjectVariable] trigger. Alias for SPLITSTRINGBYNAME to prevent issues with *ME* inserts.
The SPLITSTRING, when combined with APPENDVAR, da.him.name, SAVESETVARS, SAVEGETVARS and instructing the user to change their name, should be turing complete; you could technically make a basic in-memory dialogue editor with it.

v4.07
[PUSSYDRIP_<durationInFrames>] - Reproduces the cum effect of her masturbation. The regular effect is 50 frames long in duration. Beware of using really large numbers (1000+) because it does add quite a bit of lag.
[CLEARLINES_<linetype>] - Removes all lines from the dialogue with the matching linetype. Kinda useful for making lines play only once, but you're still left with a line-reference trigger that goes nowhere in another line.

v4.05
[CUM_BLOCK_ON] - Stops him from ejaculating. [EJACULATE] and the J-key will still force him to cum, but otherwise, the highest he can reach is 100% pleasure. See also da.blockingOrgasm variable.
[CUM_BLOCK_OFF] - Removes the cum block created by [CUM_BLOCK_ON]. See also da.blockingOrgasm variable.

v4.04
[LOAD_FULL_CHARCODE] - To load charcodes without any editing at all

v4.02
[LOAD_CHARCODE2] - To load charcodes with sby support

v4.01
[PLAY_SFX_<filename>] - plays a music/sound file as sound effect. Doesn't loop. If another sound file is started via PLAY_SFX, the previous one stops. Only supports .mp3.
[STOP_SFX] - stops the music file started by [PLAY_SFX_<filename>], if it hasn't ended already.

v4.00
[APPENDVAR_localVar1_value] - appends value to localVar1. [SETVAR_localVar1_1][APPENDVAR_localVar1_hello] will set localVar1's value to "1hello".
[APPENDVARBYNAME_localVar1_localVar2] - appends the value of localVar2 to localVar1. Basic string concatenation.
[APPENDVARBYNAEM_localVar1_localVar2] - alias for APPENDVARBYNAME to prevent issues with *ME* inserts.

v3.05
[PLAY_BGM_<filename>] - plays a music file as background music. Loops almost forever. Volume can be controlled via da.bgm.volume. If another music file is started via PLAY_BGM, the previous one stops. Only supports .mp3.
[STOP_BGM] - stops the music file started by [PLAY_BGM_<filename>].

v3.02
[SETVARBYNAEM_localVar1_localVar2] - alias for [SETVARBYNAME_localVar1_localVar2]
[SETGLOBALBYNAEM_globalVar_localVar] - alias for [SETGLOBALBYNAME_globalVar_localVar]

v3.01
[SETVOICE_voiceID] Alters the sound files used for [AH_HARD], [OH_HARD], [OH_SOFT], [OW]. Supported voiceID are 1 and 2. 1 is default.
[SETVAR_localVar_value] - Sets localVar's value to value. Replaces VA_SET_VARIABLE.
[SETVAR_localVar_+=value] - Adds value to localVar's value.
[SETVAR_localVar_-=value] - Substracts value to localVar's value.
[SETVARBYNAME_localVar1_localVar2] - Sets localVar1's value to localVar2's value. Replaces VA_SET_VARIABLEBYNAME.
[SETGLOBAL_globalVar_value] - Sets globalVar's value to value, in global space. Replaces VA_SET_GLOBALVARIABLE. For a detailed explanation about global variables, see "About variable scopes.txt" in the documentation.
[SETGLOBALBYNAME_globalVar_localVar] - Sets globalVar's value to localVar's value, in global space. Replaces VA_SET_GLOBALVARIABLEBYNAME. For a detailed explanation about global variables, see "About variable scopes.txt" in the documentation.
[COPYOBJECT_layers_targetObjectVariable._sourceObjectVariable.] Copies all variable values from sourceObjectVariable to targetObjectVariable, up to <layers> deep. 0 layers means "all layers". Keep in mind that object variable notation requires a period at the end of the variable name. See "About object variables.txt" in the documentation for further details.
[COPYOBJECT_layers_targetObjectVariable._sourceObjectVariable._USING_structInfoVariable] COPYOBJECT, but uses structInfoVariable to determine which variables need to be copied. Only variables present in both structInfoVariable and sourceObjectVariable will be copied.
[DEFINEOBJECT_objectVariableName._variableName1_variableName2_variableNameN] Creates variables objectVariableName.variableName1 and so on. Initializes them to 0. Useful for COPYOBJECT USING. See "About object variables.txt" in the documentation for usage examples.
[REGISTERGLOBALS_variable1_variable2_variableN] Copies the value of all provided variables into global variables with the same name, then links the variables together. Supports objects. See "About variable scopes.txt" in the documentation for more details about globals and linking variables.
[UNREGISTERGLOBALS_variable1_variable2_variableN] Decouples ALL of the variables listed from global space. Supports objects.
[DELETEGLOBALS_variable1_variable2_variableN] Decouples, then deletes ALL of the variables listed from global space. Supports objects.
[DELETELOCALS_variable1_variable2_variableN] Deletes ALL of the variables listed from local space. Supports objects. WARNING! Does not decouple locals! If you create a new local variable with the same name, the value of that variable will get copied to the global variable too!
[LOADGLOBALS_variable1_variable2_variableN] Loads all of the listed variables from global space into your dialogue. Links them automatically. Supports objects.
[GETGLOBALS_variable1_variable2_variableN] Loads all of the listed variables from global space into your dialogue, unlinked. Existing links are not erased. Supports objects.
[SETGLOBALS_variable1_variable2_variableN] Sets all of the listed variables from local space into the global space, unlinked. Existing links are not erased. Supports objects.
[SAVESETVARS_name_variable1_variable2_variableN] Creates a savefile with name if not exists and adds the variables to the savefile. Supports objects. See "About variable scopes.txt" in the documentation for more information regarding savefiles.
[SAVEGETVARS_name_variable1_variable2_variableN] Loads from a savefile with the supplied name the listed variables from the savefile. Supports objects.
[SAVEREMOVEVARS_name_variable1_variable2_variableN] Removes variables from a savefile. Supports objects.
[SAVEDELETE_name] Deletes a savefile.

v3.00
(Deprecated since v3.01) [VA_SAVE_SAVEGAME_<SAVENAME>] Saves all variables in global namespace to savefile with name <SAVENAME>
(Deprecated since v3.01) [VA_LOAD_SAVEGAME_<SAVENAME>] Loads all variables from savefile with name <SAVENAME>
(Deprecated since v3.01) [VA_CLEAR_SAVEGAME_<SAVENAME>] Deletes savefile with name <SAVENAME>
[BUTTON<1-10>_ON] Enables the button identified by the number, if it has button text set.
[BUTTON<1-10_OFF] Disables the button identified by the number.
[BUTTONALL_ON] Enables all buttons with button text.
[BUTTONALL_OFF] Disables all buttons.
[BUTTONALL_CLEAR] Disables all buttons and clears button text. Good for wiping menus.
[SLAP] - Sound
[AH_HARD] - Sound
[OH_HARD] - Sound
[OH_SOFT] - Sound 
[OW] - Sound
[LOAD_MOD] - Set "da.mod.load" first. Then use this trigger to load mod. See "variables.txt" for more info.
[CLEAR_MOD] - Attempts to clear all customly loaded mods. Very likely to break things.

v2.04
[HIDE_HIS_ARM] - Makes his arm disappear if he's visible. Recommend [RELEASE] to be paired with this to prevent "ghost hand".
[SHOW_HIS_ARM] - Makes his arm reappear if he's visible.

v2.03
[VA_SET_VARIABLE_<variableName>_+=<value>] - Adds the provided value to the named variable's value. Example: [VA_SET_VARIABLE_var1_+=5].
[VA_SET_VARIABLE_<variableName>_-=<value>] - Substracts the provided value to the named variable's value. Example: [VA_SET_VARIABLE_var1_-=5].

v2.00 and older (for v1 documentation, check documentation provided archive download)
[RUB_HIS_CROTCH_ON] - She starts rubbing his crotch with her right hand. You MUST call [RUB_HIS_CROTCH_OFF] before relocating the arm.
[RUB_HIS_CROTCH_OFF] - She stops rubbing his crotch.
[ARMS_CROTCH] - Places her left hand in her crotch.
[ARMS_HIS_CROTCH] -  Places her hands on his crotch.
[RIGHT_ARM_BREAST] - Places her right arm on her right breast (hand cupping breast).
[LEFT_ARM_BREAST] - Places her left arm on her left breast (hand cupping breast).
[ARMS_BREAST] - Places both her arms on her breasts (hands cupping breasts).

[FIX_ARMS] - Stops rubbing, rotates her hands back to normal
[FIX_LEFT_ARM] - FIX_ARMS, left arm only
[FIX_RIGHT_ARM] - FIX_ARMS, right arm only

[BOUNCE_TITS] - works same as before, with a power of 0.15
[BOUNCE_TITS_<power>] - BOUNCE_TITS, with custom power argument. Suggested max is 0.5.

[FLASH_<hex>] - Fades the screen to hex color.
[FLASH_<hex>_<durationInMilliseconds>] - Fades the screen to hex color, waits durationInMilliseconds milliseconds before ending the fade effect.
[INSTANT_FLASH_<hex>] - Instantly sets the screen to hex color. Only ends after [END_FLASH] or a variant thereof.
[INSTANT_FLASH_<hex>_<durationInMilliseconds>] - Instantly sets the screen to hex color, waits durationInMilliseconds milliseconds before ending the fade effect.
[END_FLASH] - ends fade effect.
[INSTANT_END_FLASH] - instantaneously ends fade effect (no fade-in)
[FLASH_CHANGE_COLOR_<hex>] - changes the fade effect's color.
[FADE_BACKGROUND_<hex>] - fades the screen to hex color, then changes the background to that set to da.background.load.
[CHANGE_BACKGROUND] - delay set for da.background.load.

[AUTO_OFF] - Turns auto mode off.
[AUTO_SOFT] - Turns auto mode on, and sets it to soft.
[AUTO_NORMAL] - Turns auto mode on, and sets it to normal.
[AUTO_HARD] - Turns auto mode on, and sets it to hard.
[AUTO_SELF] - Turns auto mode on, and sets it to self.

[HIDE_PENIS] - Hides penis (and moves tip back)
[HIDE_BALLS] - Sets balls to "None"
[SHOW_BALLS] - Sets balls to "Normal"
[SHOW_PENIS] - Shows penis, sets tip back to normal.

[CLEAN_CUM] - Cleans cum off the screen (similar to SDT's built in clean button)
[CLEAN_MASCARA] - Cleans mascara off the screen (similar to SDT's built in clean button)
[CLEAN_SPIT] - Cleans spit off the screen (similar to SDT's built in clean button)
[CLEAN_LIPSTICK] - Cleans lipstick smearing off the screen (similar to SDT's built in clean button)
[CLEAN_ALL] - Acts as [CLEAN_CUM],[CLEAN_SPIT],[CLEAN_LIPSTICK],[CLEAN_MASCARA] at the same time.

[GAG] - Plays gag sound and causes her to inversely swallow.
[MOAN] - Plays moan sound.

[WAKE_UP] - Wakes her up.
[KNOCK_OUT] - Makes her pass out (you'll have to add your own slap effect)
[DEEPTHROAT] - Gives her an intro distance of 150... which should allow her to deepthroat him.
[RANDOMIZE_HER] - Randomizes her completely.
[MASTURBATE_ON] - Makes her masturbate.
[MASTURBATE_OFF] - Makes her stop masturbating... after a while
[MASTURBATE_STOP] - instantly stops her masturbating (will still slowly drop her pleasure)

[AUTO_KEYS] - Inverts auto keys (loader setting)
[AUTO_KEYS_ON] - Sets auto keys to on (loader setting). Needed for playing of recordings and [PULL_OFF].
[AUTO_KEYS_OFF] - Sets auto keys to off, and gives player control again.
[PAUSE] - Pause current recording.
[CONTINUE] - Play/Continue current recording.
[PULL_OFF] - Pulls her off his cock. Only works properly during playing of recording.

[HIDE_HIM] - Hides him. Possibly incompatible with animtools. (Loading a position might show him weirdly if he's hidden)
[SHOW_HIM] - Makes him reappear.
[CUM] - Makes him cum quickly, but not directly (probably triggers pre_cum lines)

[LOAD_HAIR] - Loads a hair file set to the variable da.hair.load . Only works if the trigger is on the same dialogueline as the variable set for da.hair.load.
[LOAD_CHARCODE] - Loads a charcode. Is a delay set for da.charcode.load.

[VA_SET_VARIABLE_<variableName>_<value>] - Sets the named variable's value to the provided value. Example: [VA_SET_VARIABLE_var1_5].
[VA_SET_VARIABLEBYNAME_<variableTargetName>_<variableSourceName>] - Sets the named target variable's value to the value of the named source variable. Example: [VA_SET_VARIABLE_var1_var2].
[VA_SET_GLOBALVARIABLE_<globalVariableName>_<value>] - stores a value in DialogueAction's memory.
[VA_SET_GLOBALVARIABLEBYNAME_<globalVariableName>_<sourceVariableName>] - stores a value in DialogueAction's memory, using a dialogue variable's name to pull the value from.
[VA_LOAD_GLOBALVARIABLE_<globalVariableName>] - sets <globalVariableName> as a dialogue variable using the value stored in DialogueAction's memory.

[RANDOMIZE_HER_BODY] - Randomizes her body
[RESET_RESIST] - Resets her resistance. May cause first_dt, first_throat and intro lines to trigger again. Change resistances with charcode loading.

[TONGUE_OUT] - Moves her tongue out (might break through clenching teeth)
[TONGUE_IN] - Moves her tongue back in (it might pop back out 2 seconds later though)