v3.04
[<linename>_INSTANT] plays the line instantly, if it only contains triggers.

v3.00
[buttonX] (where X is 1 to 10) Triggers once a button has been pressed.

v2.00 and older (for v1 documentation, check documentation provided archive download)
[start]  Triggers once immediately after the game/mod is loaded.
[passed_out] Triggers whilst she is passed out.
[orgasm] Triggers when she climaxes through masturbation.
[choking] Triggers whilst she is starting to pass out and has her mouth full (when she starts tapping her hands automatically)