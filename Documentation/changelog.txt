v4.09dev10
- Fixed a bug with [PUSSYDRIP] without time argument since caused an error. The version with arguments worked fine.

v4.09dev9
- Breaking part of vanilla SDT compatibility; If a variable is set to a single space, rather than incrementing the variable by 0, DialogueActions will now set the variable to a single space.
- Added [SPLITSTRING_stringLiteral_localTargetObjectVariable] trigger. Assigns the length of the string literal to targetObjectVariable.length, and each of its individual characters to targetObjectVariable.charAt.<number>. Strings are 0 indexed, so "apple" will turn into charAt.0=a, charAt.1=p, charAt.2=p, charAt.3=l, charAt.4=e, length=5
- Added [SPLITSTRINGBYNAME_localVariable_localTargetObjectVariable] trigger. Performs SPLITSTRING on the contents of localVariable.
- Added [SPLITSTRINGBYNAEM_localVariable_localTargetObjectVariable] trigger. Alias for SPLITSTRINGBYNAME to prevent issues with *ME* inserts.
- Added da.her.name {read only} - *ME* as a variable.
- Added da.her.namePossessive {read only} - *MY* as a variable.
- Added da.him.name {read only} - *YOU* as a variable.
- Added da.him.namePossessive {read only} - *YOUR* as a variable.

v4.09dev8
- Fixed runTriggerCode API call to not require [ and ].
- Added ability to run multiple triggers at once via runTriggerCode API call by including [ and ] (like "[BOUNCE_TITS][BLINK]").
- Prefixed outputLog calls with DialogueActions so you can see better what logging is from DA
- Prefixed on screen messages (updateStatusCol) with "DA: " so you can see what spam is from DialogueActions

v4.09dev7
- Allow reading custom hairs set by MoreClothing with da.her.body.hair
- Fixed tan slider not updating
- Fix RGB/a and HSLC coloring issue with multiple sets to various colorables

v4.09dev6
- Fixed numerical setters for da.her.body and da.him.body variables to treat a negative value as a negative value and not a subtraction
- Added da.her.body.tan {read and write} - For setting her tan type. Values are "None", "Full", "Bikini", "Swimsuit" and "Swimsuit-T".
- Added da.her.body.tan.amount {read and write} - For setting the amount of her tan - acts as an alpha factor for the tan sprite? Values range from 0 to 1.

v4.09dev5
- Added da.her.body.hair {read and write} - For setting her hair. Values are the same strings shown in the in-game menu and can be expanded via MoreClothing. Doesn't reflect custom png hairs.
- Added da.him.body.penis.length {read and write} - Controls his penis length. Values range from 0.7 to 1.2 normally, but with mods this can change. See minSize and maxSize variables.
- Added da.him.body.penis.width {read and write} - Controls his penis width. Values range from 0.7 to 1.2 normally, but with mods this can change. See minSize and maxSize variables.
- Added da.him.body.penis.minSize {read only} - Retrieves his min penis size. Useful in combination with penisrange mod.
- Added da.him.body.penis.maxSize {read only} - Retrieves his max penis size. Useful in combination with penisrange mod.
- Added da.him.body.penis {read and write} - Controls his penis type. Values are "Penis A", "Penis B" and "Strapon".
- Added da.her.body.penis {read and write} - Controls her penis type. Values are "None", "Penis A", "Penis B" and "Strapon".
- Added da.her.body.penis.length {read and write} - Controls her penis length. Values range from 0.7 to 1.2 normally, but with mods this can change. See minSize and maxSize variables.
- Added da.her.body.penis.width {read and write} - Controls her penis width. Values range from 0.7 to 1.2 normally, but with mods this can change. See minSize and maxSize variables.
- Added da.her.body.penis.minSize {read only} - Retrieves her min penis size. Useful in combination with penisrange mod.
- Added da.her.body.penis.maxSize {read only} - Retrieves her max penis size. Useful in combination with penisrange mod.

v4.09dev4
- Added default value for [PUSSYDRIP_<durationInFrames>] trigger, which is 50 frames because that's what the masturbation has been using.
- Added da.him.body.gender {read and write} - Controls his body type. Values are "Male" and "Female".
- Added da.him.body.skin {read and write} - Controls his skin type. Values are, for the Male body, "Light" and "Dark"; for the Female body, "Light", "Pale", "Tan" and "Dark".
- Added da.him.body.balls {read and write} - Controls his balls type. Values are "None" and "Normal".
- Added da.him.body.balls.size {read and write} - Controls his balls size. Values range from 0.5 to 1.3, but mods may alter the minimum and maximum sizes.
- Added da.him.body.breasts {read and write} - Controls his breast size. Values range from 1 to 149, but for the Male body, the value is always 0 since there are no breasts.
- Added da.him.ejaculating {read only} - Whether he's currently ejaculating. Values are "true" and "false".

v4.09dev3
- Added da.her.body.mascara {read and write} - Controls her ... mascara slider. Scale goes from 0 to 100. My best guess for the mascara slider is that it's the chance that a tear will cause a smear. There is no change in the intensity of the mascara. For intensity changes, try looking at the alpha colorcomponent.
- Added da.her.body.freckles {read and write} - Controls the freckles on her face. Scale goes from 0 to 100. Rather than being a fade-in, freckles seem to work by drawing more freckles as the slider goes up.
- Added da.her.body.eyebrow {read and write} - Controls her eyebrow type. Values are "Normal", "Crescent" and "Lines".
- Added da.her.clenchingTeeth {read only} - Whether she's clenching her teeth.
- Added HSLC coloring for her skin, his skin and her hair.

v4.09dev2
- Added da.her.body.iris {read and write} - Controls her iris type. Values are "Normal", "Bright", "Solid", "Blank", "Wide", "Sharp", "Flat", "Cat" and "Swirl".
- Added da.her.body.iris.<colorComponent> {read and write} - For coloring her iris, colorComponents r,g,b,a. RGB go from 0 to 255 and A goes from 0 to 1.
- Added da.her.body.sclera.<colorComponent> {read and write} - For coloring her sclera, colorComponents r,g,b,a. RGB go from 0 255 and A goes from 0 to 1.
- Added a bunch of other stuff that I will properly document later in the changelog when this properly gets released.
- Fixed da.her.body.breasts to go from 0-149.

v4.09dev
- Added da.her.body.breasts {read and write}. Controls her breast size. Scale goes from 0-149. It "pops" when setting, so recommend combining with [BOUNCE_TITS] to hide the instant growth, or use small increments.
- Added da.her.body.scale {read and write} - Controls her body scale. Goes from 0 to 1. I recall being able to go over 1, but SDT doesn't seem to play along.
- Added da.her.body.skin {read and write} - Controls her skin type. Values are "Light", "Pale", "Tan" and "Dark".
- Added da.her.body.ears {read and write} - Controls her ear type. Values are "Normal", "Elf" and "Small".
- Added da.her.body.nose {read and write} - Controls her nose type. Values are "Normal", "Pointed" and "Wedge".

v4.08
- Fixed a bug with da.dialogue.load introduced in v4.07.

v4.07
- Fixed the use of checks (line-attribute check) so that they now work.
- Fixed a bug where sets would write to the dialogue variable scope even if a hook for that variable exists, which confused a lot of people
- Added [PUSSYDRIP_<durationInFrames>] trigger. This reproduces the effect you get when she cums from masturbation.
- Added support for targeting other character folders than the current one with filereferences through prefixing with a dollar sign. See "About filereferences.txt" for more details.
- Added da.dialogue.import {write only} variable which allows you to read a dialogue file and append its lines to the current dialogue. 
- Added [CLEARLINES_<linetype>] trigger. This trigger clears all lines of the specified linetype.
- Added da.sdtoptions variables (see "Variables.txt" for a list). These allow you to modify all of the boolean options in the options menu.

v4.06
- Updated known bugs list with 2 entries, one for da's variables in check line-attributes not working, one for da.finishes being swallowed in any line with line-attributes
- Added API function registerVariableReadHandler, which allows you to expose dialogue variables.
- Updated API Documentation with small descriptions (one-liners) as to what you'd use the functions for.
- Fixed a bug where lines with a line name that is 7 characters long ("general", "pre_cum", "restart", "swallow", "choking", button1 through 9) that only contains triggers was played instantly even although they shouldn't have

v4.05
- Fixes a bug with [DELETELOCALS_<vars>] so that it actually works
- Make non-existing sound files give a red message, rather than an error
- Added [CUM_BLOCK_ON] and [CUM_BLOCK_OFF], as well as da.blockingOrgasm to stop him from cumming

v4.04
- Added [LOAD_FULL_CHARCODE], which doesn't alter character codes that get loaded at all and directly passes them to SDT.
- Added addLineChangeListener to API functions, to allow other mods to listen when a line is changed because this was previously not possible due to the way DA handles instant-lines.
- Fixes a bug with loading non-existing saves.

v4.03
- Fixed a bug with start lines firing upon starting of SDT with an old dialogue; this caused an error popup for anyone with a debug flash player and it was annoying in general; it also broke MoreClothing every now and then because of this
- Fixed [LOAD_MOD] - it now properly loads from the last loaded character folder and not $OVER$ like it did.

v4.02
- Reuploaded v4.01 to bitbucket because it somehow got murdered
- Added [LOAD_CHARCODE2], which uses what this post said (https://www.undertow.club/threads/pim_gds-mods-dialogueactions-v4-01-7-aug-2015.5889/page-16#post-114156): "yes, we could simply remove the CharCodeAction.as::loadCharCode(...) method and invoke g.InGameMenu.loadData(...) instead." To load charcodes with sby support

v4.01
- Fixed a bug where switching dialogues would cause global variables to be automatically imported into the next dialogue, and values to be overwritten by initial_settings values.
- Added api functions:
-- registerVariableWriteListener allows you to recieve value updates from dialogue variables
-- registerVariableWriteHandler allows you to intercept variable writes, exposing your own functions as dialogue variables
-- getVariableValue allows you to retrieve a variable value as seen in variable insertions
-- setVariableValue allows you to set a variable value as seen in the set line-attribute
-- See Documentation/For Modders/Api.txt for more info
- Added [PLAY_SFX_<filename>]. Plays a mp3 file from the last loaded character folder. Only 1 sound and 1 bgm can play at the same time for now.
- Added [STOP_SFX]. Stops the sound started by PLAY_SFX, if it hasn't ended yet.
- Fixed a bug in [STOP_BGM] where it would just crash DA.
- Added da.finishes - read only variable to retrieve cum counter.

v4.00
- Migrated project to BitBucket/Git!
- added [APPENDVAR_localVar1_value] - appends value to localVar1. [SETVAR_localVar1_1][APPENDVAR_localVar1_hello] will set localVar1's value to "1hello".
- added [APPENDVARBYNAME_localVar1_localVar2] - appends the value of localVar2 to localVar1. Basic string concatenation.
- added [APPENDVARBYNAEM_localVar1_localVar2] - alias for APPENDVARBYNAME to prevent issues with *ME* inserts.
- removed cleaning at low FPS.
- Added "registervariableWrite" to API. This allows you to make on-screen dialogue variable monitors - it's a onVariableWrite listener.

v3.06
- Fixed a bug with the values of da.breathPercentage, now it works properly (previously read gave bogus values and set did nothing due to a typo)
- Removed white debug messages for loading variables from a savefile

v3.05
- Added "registerTrigger" to API. This function allows you to hook one of your functions up to a dialogue trigger.
- Added [PLAY_BGM_<filename>]. This plays a music file from the last loaded character folder.
- Added [STOP_BGM]. This stops playing the music file started by [PLAY_BGM_<filename>].
- Added da.bgm.volume {write only}. Range 0 (mute) to 1 (full volume, default). This sets the volume of the background music started with [PLAY_BGM_<filename>].

v3.04
- Fixed a bug with da.herPleasure where setting to a number took a value between 0 and 1 instead of 0-100.
- Added da.breathPercentage for her breath level {read and write} (this drops first)
- Added da.oxygenPercentage for her oxygen level {read and write} (this drops when she starts passing out)
- Added da.canSpeakLineTrigger for whether speak lines can trigger {read only} (based on gag, mouth full, passed out, and swallowing).
- Added a feature where any line that ends with "_INSTANT" (as in "intro4_INSTANT") and only contains triggers is resolved instantly. This means you can do 100 triggers for setting variables and such in a single frame.
- Added Mod Communication (Mod comms). See For "Modders/Api.txt". DialogueActions uses "DialogueActions" as api key.
-- Added 1 function "runTriggerCode". Input is a string, like a trigger but without the brackets. 

v3.03
- Fixed a bug with [SETVAR_localVar_-=number] that didn't work properly for positive numbers
- Fixed a bug with [SETVAR_localVar_+=number] that didn't work properly for negative numbers

v3.02
- Added SETVARBYNAEM and SETGLOBALBYNAEM aliases for SETVARBYNAME and SETGLOBALBYNAME due to *a*[ME*b*] bug (they do the same thing but prevent the bug)
- Added read/write variable da.pleasurePercentage, goes from 0 to 100, sets him pleasure. Setting it at 100 won't make him cum, you'll need the girl to finish the job.

v3.01 (by Pimgd/WeeWillie)
- Deprecated all triggers starting with "VA_" (VA_SET_GLOBALVARIABLE, VA_SET_GLOBALVARIABLEBYNAME, VA_LOAD_GLOBALVARIABLE, VA_SET_VARIABLE, VA_SET_VARIABLEBYNAME, VA_SAVE_SAVEGAME, VA_LOAD_SAVEGAME, VA_CLEAR_SAVEGAME)
-- Deprecated triggers can still be used, but they may not perform optimally.
-- VA_SET_VARIABLE and related triggers have bugs regarding setting of positive integers
-- VA_SET_GLOBALVARIABLE and related triggers use a separate storage from SETGLOBAL.
- Defines a new concept: Object variables. See "About Object variables.txt" in the documentation.
- New triggers:
-- [SETVOICE_voiceID] Alters the sound files used for [AH_HARD], [OH_HARD], [OH_SOFT], [OW]. Supported voiceID are 1 and 2. 1 is default.
-- [SETVAR_localVar_value] - Sets localVar's value to value. Replaces VA_SET_VARIABLE.
--- [SETVAR_localVar_+=value] - Adds value to localVar's value.
--- [SETVAR_localVar_-=value] - Substracts value to localVar's value.
-- [SETVARBYNAME_localVar1_localVar2] - Sets localVar1's value to localVar2's value. Replaces VA_SET_VARIABLEBYNAME.
-- [SETGLOBAL_globalVar_value] - Sets globalVar's value to value, in global space. Replaces VA_SET_GLOBALVARIABLE. For a detailed explanation about global variables, see "About variable scopes.txt" in the documentation.
-- [SETGLOBALBYNAME_globalVar_localVar] - Sets globalVar's value to localVar's value, in global space. Replaces VA_SET_GLOBALVARIABLEBYNAME. For a detailed explanation about global variables, see "About variable scopes.txt" in the documentation.
-- [COPYOBJECT_layers_targetObjectVariable._sourceObjectVariable.] Copies all variable values from sourceObjectVariable to targetObjectVariable, up to <layers> deep. 0 layers means "all layers". Keep in mind that object variable notation requires a period at the end of the variable name. See "About object variables.txt" in the documentation for further details.
-- [COPYOBJECT_layers_targetObjectVariable._sourceObjectVariable._USING_structInfoVariable] COPYOBJECT, but uses structInfoVariable to determine which variables need to be copied. Only variables present in both structInfoVariable and sourceObjectVariable will be copied.
-- [DEFINEOBJECT_objectVariableName._variableName1_variableName2_variableNameN] Creates variables objectVariableName.variableName1 and so on. Initializes them to 0. Useful for COPYOBJECT USING. See "About object variables.txt" in the documentation for usage examples.
-- [REGISTERGLOBALS_variable1_variable2_variableN] Copies the value of all provided variables into global variables with the same name, then links the variables together. Supports objects. See "About variable scopes.txt" in the documentation for more details about globals and linking variables.
-- [UNREGISTERGLOBALS_variable1_variable2_variableN] Decouples ALL of the variables listed from global space. Supports objects.
-- [DELETEGLOBALS_variable1_variable2_variableN] Decouples, then deletes ALL of the variables listed from global space. Supports objects.
-- [DELETELOCALS_variable1_variable2_variableN] Deletes ALL of the variables listed from local space. Supports objects. WARNING! Does not decouple locals! If you create a new local variable with the same name, the value of that variable will get copied to the global variable too!
-- [LOADGLOBALS_variable1_variable2_variableN] Loads all of the listed variables from global space into your dialogue. Links them automatically. Supports objects.
-- [GETGLOBALS_variable1_variable2_variableN] Loads all of the listed variables from global space into your dialogue, unlinked. Existing links are not erased. Supports objects.
-- [SETGLOBALS_variable1_variable2_variableN] Sets all of the listed variables from local space into the global space, unlinked. Existing links are not erased. Supports objects.
-- [SAVESETVARS_name_variable1_variable2_variableN] Creates a savefile with name if not exists and adds the variables to the savefile. Supports objects. See "About variable scopes.txt" in the documentation for more information regarding savefiles.
-- [SAVEGETVARS_name_variable1_variable2_variableN] Loads from a savefile with the supplied name the listed variables from the savefile. Supports objects.
-- [SAVEREMOVEVARS_name_variable1_variable2_variableN] Removes variables from a savefile. Supports objects.
-- [SAVEDELETE_name] Deletes a savefile.

- Added write-only variables da.button<1-10>.varname. Useful for setting button names instantly via the set line-attribute; works the same as [SETVARBYNAME_da.button<1-10>.name_localVarName].
- Fixed a bug that caused choking lines not to play properly (they used to play only if she was at more than 100% oxygen instead of less >_<)

v3.00 (by WeeWillie, 4/17/14)
- Added [VA_SAVE_SAVEGAME_<SAVENAME>],[VA_LOAD_SAVEGAME_<SAVENAME>], which save to disk any global variables set by [VA_SET_GLOBALVARIABLE...]
- Added [VA_CLEAR_SAVEGAME_<SAVENAME>] which deletes a save game made via [VA_SAVE_SAVEGAME_<SAVENAME>]
- When DialogueActions first loads, it clears out the active dialog.  This is to avoid issues where a dialog is loaded through the character menu (i.e. Scene->Custom), and then on next load of SDT automatically runs without having the needed folder initializations.
- When the frame rate gets 10fps or below, DA clears the spit, and if fps still doesn't rise, it clears the cum strands in an attempt to correct the poor framerate.  When frame rate is low, other commands like animtools and character loads begin to fail.
- Added [BUTTON<1-10>_ON][BUTTONALL_ON][BUTTON<1-10_OFF][BUTTONALL_OFF], used in conjuction with "da.button<1-10>.name" for a button GUI. These trigger button<1-10> lines when pressed.
- Added audio triggers, [SLAP],[AH_HARD],[OH_HARD],[OH_SOFT], and [OW].
- Added ability to load a mod via dialog using [LOAD_MOD] and [CLEAR_MOD] in conjuntion with "da.mod.load"

v2.04
- Fixed a bug with string comparison and parentheses.
- Added [HIDE_HIS_ARM], which hides (and only hides) his arm. Recommend [RELEASE] to be paired with this.
- Added [SHOW_HIS_ARM].

v2.03
- Fixed [GAG] causing a internal crash which led to line freeze and infinite choking
- Altered the way [VA_SET_VARIABLE_<var1>_<value>] works: [VA_SET_VARIABLE_<var1>_+=<value>] for adding numbers, [VA_SET_VARIABLE_<var1>_-=<value>] for substracting numbers, [VA_SET_VARIABLE_<var1>_<value>] for directly setting the value (which will cast string to number)

v2.02
- Fixed a bug in equation evaluation where *4 + 3* would turn into 43. (should be 7)

v2.01
- Turned off debug messages (left them on by accident)
- Implemented string comparison (but if you have spaces in a string they will get ignored)

v2.00
- Fixed a bug where a trigger could get skipped due to low framerate
- Fixed a bug where triggers at the start of the line would get skipped
- Fixed a bug where her masturbation would continue if the game was paused
- Ending masturbation with [MASTURBATE_OFF] now no longer changes her right arm position (if changed by animtools)
- Special characters (such as periods) are now no longer filtered from triggers for DialogueActions.
- Merged VariableArithmetic into DialogueActions

- Changed the way backgrounds, charcodes, dialogues and hair files are loaded
-- (variable) background {write only} -> (variable) da.background.load {write only}
-- (variable) loadCharCode {write only} -> (variable) da.charcode.load {write only}
-- (variable) hair {write only} -> (variable) da.hair.load {write only}
-- (variable) loadDialogue {write only} -> (variable) da.dialogue.load {write only}

- Changed the way clothing is set
-- (variable) <clothingType> {write only} -> (variable) da.clothes.<clothType> {read AND write} (clothType later explained in greater detail)

- Changed triggers:
-- [FLASH_<hex>] - Fades the screen to hex color, lasts indefinitely
-- any triggers with arm resetting may cause minor graphical glitches. Fixable with [FIX_ARMS] ([FIX_LEFT_ARM][FIX_RIGHT_ARM])

- Added variables:
da.masturbation.herpleasure {read and write} - her masturbation pleasure (scale: 0-100)
da.random {read only} - returns a random value between 0 and 1. Example usage to make it 1-10: (you'll have to define x as "*" in initial_settings to get access to multiplication operator) [VA_SET_VARIABLE_var1_*da.random x 9 + 1 \ 1*] - formula is ( random * ( max - min ) ) + min \ 1
da.hair.load {write only} - For loading hair files. Only static images are supported right now. Delay set is possible, using [LOAD_HAIR]
da.background.load {write only} - For loading backgrounds. Delay set is possible, using either [CHANGE_BACKGROUND] or [FADE_BACKGROUND_<hex>]
da.dialogue.load {write only} - For loading dialogues. Acts instantly, might halt the current line from playing.
da.charcode.load {write only} - For loading (partial) charcodes.
da.clothes.<clothType>.<colorComponent> {read and write} 
--clothType = any of these: "ankleCuffs", "armwear", "bellyPiercing", "bottoms", "collar", "cuffs", "earring", "eyewear", "footwear", "gag", "headwear", "himBottoms", "himFootwear", "himTop", "legwear", "legwearB", "nipplePiercing", "panties", "tonguePiercing", "top", "tops"
--colorComponent = any of these: "r", "g", "b", "a", "r2", "g2", "b2", "a2". r, g, b and their r2, g2, b2 cousins are from 0 to 255. a and a2 are from 0 to 1

- Added triggers:
-- [BOUNCE_TITS_<power>] - BOUNCE_TITS, with custom power argument. Suggested max is 0.5. [BOUNCE_TITS] uses 0.15.
-- [FIX_ARMS] - Stops rubbing, rotates her hands back to normal
-- [FIX_LEFT_ARM] - FIX_ARMS, left arm only
-- [FIX_RIGHT_ARM] - FIX_ARMS, right arm only
-- [INSTANT_END_FLASH] - Instantly ends a FLASH effect, without fading
-- [INSTANT_FLASH_<hex>] - Instantly sets the screen to hex color. Only ends after [END_FLASH] or a variant thereof.
-- [INSTANT_FLASH_<hex>_<durationInMilliseconds>] - Instantly sets the screen to hex color, waits durationInMilliseconds milliseconds before ending the fade effect.
-- [FLASH_CHANGE_COLOR_<hex>] - changes the fade effect's color.
-- [LOAD_HAIR] - Loads a hair file set to the variable da.hair.load . Only works if the trigger is on the same dialogueline as the variable set for da.hair.load.
-- [VA_SET_VARIABLE_<variableName>_<value>] - Sets the named variable's value to the provided value. Example: [VA_SET_VARIABLE_var1_5].
-- [VA_SET_VARIABLEBYNAME_<variableTargetName>_<variableSourceName>] - Sets the named target variable's value to the value of the named source variable. Example: [VA_SET_VARIABLE_var1_var2].
-- [VA_SET_GLOBALVARIABLE_<globalVariableName>_<value>] - stores a value in DialogueAction's memory.
-- [VA_SET_GLOBALVARIABLEBYNAME_<globalVariableName>_<sourceVariableName>] - stores a value in DialogueAction's memory, using a dialogue variable's name to pull the value from.
-- [VA_LOAD_GLOBALVARIABLE_<globalVariableName>] - sets <globalVariableName> as a dialogue variable using the value stored in DialogueAction's memory.
-- [RANDOMIZE_HER_BODY] - Randomizes her body
-- [RESET_RESIST] - Resets her resistance. May cause first_dt, first_throat and intro lines to trigger again. Change resistances with charcode loading.
-- [TONGUE_OUT] - Moves her tongue out (might break through clenching teeth)
-- [TONGUE_IN] - Moves her tongue back in (it might pop back out 2 seconds later though)
-- [MASTURBATE_STOP] - instantly stops her masturbating (will still slowly drop her pleasure)

- Added linetypes:
-- "choking" - plays when her mouth is full and she is passing out (starting to look like she is passing out, breath at 0 and oxygen dropping)

- Added the following operators:
Operator	var1	var2	output
+		10	20	30 - Adds both sides together.
-		10	20	-10 - Substracts the right side from the left side.
/		10	20	0.5 - Divides the left side by the right side. If the right side is 0, ... then I don't know what happens. Check Adobe's manual for division by 0.
!=		10	20	1 - If both sides don't have the same value, returns 1. Returns 0 otherwise.
==		10	20	0 - If both sides have the same value, returns 1. Returns 0 otherwise.
>=		10	20	0 - If the left side is more than, or equal to the right side, returns 1. Otherwise returns 0.
<=		10	20	1 - If the right side is more than, or equal to the left side, returns 1. Otherwise returns 0.
>		10	20	0 - If the left side is more than the right side, returns 1. Otherwise returns 0.
<		10	20	1 - If the right side is more than the left side, returns 1. Otherwise returns 0.
=		10	20	20 - Whatever the result might have been, it's set to the right side.
&&		1	0	0 - Represents a boolean AND. Returns 1 if (var1 + var2) equals 2. Otherwise, returns 0.
||		1	0	1 - Represents a boolean OR. Returns 1 if (var1 + var2) equals or is more than 1. Otherwise, returns 0.	
%		10	4	2 - Returns the remainder of a division of var1 by var2. Represents a modulo.
*		10	20	200 - Returns var1 multiplied by var2. Note that you can't use this operator directly - you need to define a variable with * as its value.
\		10	3	3 - Returns the floored value of a division of var1 by var2. Represents integer division.

You can use ( ) to specify ordering (other ordering is ignored).
You can use variables names inside the insertion *var1 + var2* and they will automatically be replaced with the value that they have *var1 + var2* -> *10 + 20* -> 30
Keep in mind that for this to work properly, variable names CANNOT have spaces in them, and variables CANNOT be attached to parentheses or an operator.
Thus, don't use *var1+(var2/var3)*, instead use *var1 + ( var2 / var3 )*.
Additionally, due to the way replacing of insertion values work, variable names CANNOT contain operators.


- Removed the following triggers:
-- [SHRINK_PENIS]
-- [GROW_PENIS]
-- [CLEAR] - Load an empty dialogue with da.dialogue.load

- Removed the following variables:
-- penisSize - write-only, scale 0-100
-- penisLength - write-only, scale 0-100
-- penisWidth - write-only, scale 0-100
-- maxPenisLength - write-only, scale unknown (0-100?)
-- maxPenisWidth - write-only, scale unknown (0-100?)
-- minPenisLength - write-only, scale unknown (0-100?)
-- minPenisWidth - write-only, scale unknown (0-100?)

See included documentation for basic examples on usage.

v1.15
- Fixed a bug that caused [FADE_BACKGROUND_<hex>] to blow up everytime you used it.

v1.14
- Fixed a bug where no checks for brackets were done (leading to [CUM] and CUM working the same)

v1.13
- Fixed a bug where defining alpha values for rgbrgb values (like footwear) would cause those to be used in the coloring, leading to unexpected colors.

v1.12
- Fixed a bug where making use of [FADE_BACKGROUND_<hex>], [FLASH_<hex>] or [FLASH_<hex>_<timeInMilliseconds>] would prevent SDT from registering mouse clicks.
- Fixed loadCharCode iris color issue
- Fixed loadCharCode hairhsl issues.

v1.11
- Added loadDialogue as variable. Write only, this variable can be set to instantaneously load a dialogue with the given name. uses "Mods/"+lastLoadedCharacterFolder+"/"+loadDialogueVariableValue.
- Changed how loadCharCode works internally - it now uses decompiled code from SDT to parse the char code, and applies the charcode. This should fix the background disappearing, but it might introduce different issues. We'll see.

v1.10
- Changed variable nuking for non-clothes variables. They are now set to empty string (""). Additionally, empty string values are ignored.
- Fixed a crash where loading a dialogue that contained loadCharCode in the initial settings would cause Flash to throw null pointer errors.

v1.09
- Added instant line redirects! Basically, when all a line consists of is a trigger, the line will be played instantaneously. This makes callback functions invisible to the end user.
- Changed the way DialogueActions loads external files. New path is "Mods/"+lastLoadedCharacterFolder+"/"+suppliedPath. This means you can place DialogueActions in the $INIT$ folder, and keep your backgrounds/hairs in the character folder.
- Fixed a bug where loading a hair that was a normal hairfile would result in an error message about "Don't load SWF hairs! EXPLOSIONS IMMINENT".

v1.08
- Added [PULL_OFF] as trigger.

v1.07
- Added [CLEAN_CUM] as trigger.
- Added [CLEAN_SPIT] as trigger.
- Added [CLEAN_LIPSTICK] as trigger.
- Added [CLEAN_MASCARA] as trigger.
- Added [CLEAN_ALL] as trigger.
- Added [HIDE_HIM] as trigger.
- Added [SHOW_HIM] as trigger.
- Added [LOAD_CHARCODE] as trigger. Only supports vanilla hairstyles and backgrounds.
- Added loadCharCode as variable. Write-only. Only supports vanilla hairstyles and backgrounds.
- Added [FLASH_<hex>_<timeInMilliseconds>] as trigger.
- Added [FLASH_<hex>] as trigger.
- Added [END_FLASH] as trigger.
- Added [RANDOMIZE_HER] as trigger.
- Fixed a bug where calling [FADE_BACKGROUND_<hex>] multiple times with different colors would blend the colors.

v1.06
- Altered "passed_out" linetype to trigger constantly whenever she's passed out.

v1.05
- Removed accidental leftover debug message

v1.04
- Fixed [LEFT_ARM_HAND_JOB] being broken.
- Added "passed_out" as linetype - is triggered once when she passes out (and makes the urkgh noise). To reset the trigger for this line (so that it will play again), she has to breathe a couple times.

v1.03
- Fixed bug where coloring and equipping ankleCuffs in one line didn't result in colors being applied

v1.02
- Added hair as variable - write-only, filename referring to file in the same folder as dialogueactions. Don't load dynamic hairs with this.

v1.01
- Added himBottoms as clothing variable. Write-only.
- Added himFootwear as clothing variable. Write-only.
- Added himTop as clothing variable. Write-only.

v1.00
- Deprecated [HAND_ON], [HAND_OFF], [HANDJOB_ON] and [HANDJOB_OFF]
- Added [RUB_HIS_CROTCH_ON] as trigger.
- Added [RUB_HIS_CROTCH_OFF] as trigger.
- Added [RIGHT_ARM_BREAST] as trigger.
- Added [LEFT_ARM_BREAST] as trigger.
- Added [ARMS_BREAST] as trigger.
- Added [AUTO_KEYS_ON] as trigger.
- Added [AUTO_KEYS_OFF] as trigger.
- Added [AUTO_KEYS] as trigger.
- Added [SHRINK_PENIS] as trigger.
- Added penisSize as variable - write-only, scale 0-100
- Added penisLength as variable - write-only, scale 0-100
- Added penisWidth as variable - write-only, scale 0-100
- Added maxPenisLength as variable - write-only, scale unknown (0-100?)
- Added maxPenisWidth as variable - write-only, scale unknown (0-100?)
- Added minPenisLength as variable - write-only, scale unknown (0-100?)
- Added minPenisWidth as variable - write-only, scale unknown (0-100?)
- Fixed clothes variables that had capitals in them.
- Added background as variable - write-only, filename referring to file in the same folder as dialogueactions
- Added [CHANGE_BACKGROUND] as trigger
- Added [FADE_BACKGROUND_<hex>] as trigger - hex is 6-character hexadecimal string (do not put # or 0x in front).
-- Example: [FADE_BACKGROUND_ffffff] - will fade to white before swapping backgrounds

v0.99
- Added [BOUNCE_TITS] as trigger (or tag, that's what gollum used to call it)

v0.98.2
- Fixed [HAND_ON], [HAND_OFF], [HANDJOB_ON], [HANDJOB_OFF]. Recommend that you use a variant of [ARMS_HAND_JOB] instead of [HANDJOB_ON].

v0.98.1
- Made DialogueActions load with v5.25c of the loader.
- Fixed clothes variables.

v0.97 Hotfix 2.1
- fixed bad property call in [MASTURBATE_ON]

v0.97 Hotfix 2
- fixed issue when going from handjob to [MASTURBATE_ON]
- fixed issues with new hand system

v0.97
- added [PAUSE] as tag.
- added [CONTINUE] as tag.
- added [CLEAR] as tag.
- added [ARMS_CROTCH] as tag.
- added [ARMS_HIS_CROTCH]  as tag.
- added general as triggerline.
- added orgasm as triggerline.
- added JSON-tags for her clothes.

v0.95
- added [MOAN] as tag.
- added [GROW_PENIS] as tag.
- added start as triggerline.
- fixed code in [HIDE_PENIS]. Now it's "gone" instead of just invisible.
- fixed code in [SHOW_PENIS]. Counteract HIDE_PENIS fix.
- changed [MASTURBATE_ON]. Now plays moaning sound when she climaxes.

v0.93
- added [AUTO_NORMAL] as tag.
- added [AUTO_SOFT] as tag.
- added [AUTO_HARD] as tag.
- added [AUTO_SELF] as tag.
- added [AUTO_OFF] as tag.

v0.92
- added [HIDE_PENIS] as tag.
- added [SHOW_PENIS] as tag.
- added [HIDE_BALLS] as tag.
- added [SHOW_BALLS] as tag.

v0.9
- added [MASTURBATE_ON] as tag.
- added [MASTURBATE_OFF] as tag.

v0.8
- added [GAG] as tag.
- added [WAKE_UP] as tag.
- added [DEEPTHROAT] as tag.
- added [CUM] as tag.
- added [HAND_ON] as tag.
- added [HAND_OFF] as tag.
- added [HANDJOB_ON] as tag.
- added [HANDJOB_OFF] as tag.