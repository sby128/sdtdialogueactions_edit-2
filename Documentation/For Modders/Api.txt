DialogueActions uses "DialogueActions" as apikey.

Functions:

v4.06
registerVariableReadHandler(name:String, callbackFunction:Function, callbackTarget:Object, callbackArgs:Array)
- For exposing variables to a dialogue.
-- name is your variable name ("playername")
-- callback function is a function that will get called when the variable is read from via the dialogue - in insertions ONLY (function signature is ():* , return the value for use in dialogue)
-- callback target and callback args, see Function.apply in AS3 docs.
--- DA uses function.apply.
-- callback args is what will be passed to your function.

v4.04
addLineChangeListener(callbackFunction:Function, callbackTarget:Object, callbackArgs:Array)
- Allows you to register a line change listener. This listener calls the callback function whenever the next line is played. Then the listener is removed.
-- callback function is a function that will get called after a line change has been detected (function signature is ():void )
-- you will not get any arguments in your callback function, you will have to get to the current line yourself
-- callback target and callback args, see Function.apply in AS3 docs.
--- DA uses function.apply.
-- callback args is what will be passed to your function.
-- Keep in mind that your line change listener will be REMOVED upon line change - you will have to re-register your line change listener for every line played if you want to somehow catch them all
--- if that's inconvenient, I can maybe change this, for now, I think you'll only want to wait for the next line change

v4.01
registerVariableWriteListener(name:String, callbackFunction:Function, callbackTarget:Object, callbackArgs:Array)
- Allows you to get notified when a dialogue variable is set.
-- name is your variable name ("myvariable")
-- callback function is a function that will get called after the variable is written to via the dialogue (function signature is (oldValue:*, newValue:*):void )
--- if the variable doesn't exist or doesn't have a value when written to, oldValue will be null
-- callback target and callback args, see Function.apply in AS3 docs.
--- DA uses function.apply.
-- callback args is what will be passed to your function.

getVariableValue(name:String):*
- Retrieves a variable value.
-- name is the variable name you want to know the value of
-- return type can be anything - null, String, or Number.

setVariableValue(name:String, value:*):void
- Sets a variable value.
-- name is the variable name you want to set
-- value is the value you want to set (for numbers, use numerical types, do not pass string unless you wish to substract. If you wish to add, pass "+value")
-- return type is void

v4.00
registerVariableWriteHandler(name:String, callbackFunction:Function, callbackTarget:Object, callbackArgs:Array)
- Allows you to intercept dialogue variable writes.
-- name is your variable name ("playername")
-- callback function is a function that will get called when the variable is written to via the dialogue (function signature is (value:*):* , return something if you want to prevent DA from setting the value)
-- callback target and callback args, see Function.apply in AS3 docs.
--- DA uses function.apply.
-- callback args is what will be passed to your function.


v3.05
registerTrigger(name:String, argumentCount:int, callbackFunction:Function, callbackTarget:Object, callbackArgs:Array)
- Allows you to expose custom triggers to the dialogue.
-- name is your trigger name ("BOUNCE_TITS")
-- argument count is the number of arguments it takes. -1 for infinite, 0 for 0, 1 for 1... ("BOUNCE_TITS_<power>" would use 1)
-- callback function is a function that will get called when the trigger is passed through DA (You'll get your parameters as arguments, "BOUNCE_TITS_<power>" when encountered as [BOUNCE_TITS_0.5] would call your function with "0.5" as the first argument)
-- callback target and callback args, see Function.apply in AS3 docs.
-- will throw an Error if the trigger is already registered for the provided amount of arguments.
--- DA uses function.apply.
-- callback args is what will be passed to your function.

v3.04
runTriggerCode(string):void - takes the contents of a trigger, like "BOUNCE_TITS". Then treats it as if it just read it from SDT. Will execute all triggers at once, just like an _INSTANT line. See the dialogue writer documentation for all the things you can do with this.
- Allows you to "trigger" triggers from code. Useful for combining several triggers into one custom trigger when combined with registerTrigger.
- Since v4.09dev8, allows multiple triggers in one go.
- Format for single is either "BOUNCE_TITS" or "[BOUNCE_TITS]".
- Format for multiple is "[BOUNCE_TITS][BLINK]"